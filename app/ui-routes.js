app.config(function($stateProvider, $urlRouterProvider) {


    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/home");
    //
    // Now set up the states
    $stateProvider
        .state('home', {
            url: "/home",
            templateUrl: "app/views/home.html"
        });
        //.state('cards.card', {
        //    url: "/card/{id}",
        //    templateUrl: "app/views/cards/card-full-template.html",
        //    params: {
        //        card: null
        //    }
        //}),

})